package com.jiuyun;

import com.jiuyun.dao.StuentDao;
import com.jiuyun.dao.imol.StuentDaoImol;
import com.jiuyun.entity.Student;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.helpers.XSSFXmlColumnPr;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

public class ExcelTest {
     public static void main(String[] args) throws IOException, SQLException {
         StuentDao studao=new StuentDaoImol();
         ClassLoader classLoader=ExcelTest.class.getClassLoader();
         //
         String filename="hello.xlsx";
         InputStream input=classLoader.getResourceAsStream(filename);
         //打开excel
         XSSFWorkbook sheets=new XSSFWorkbook(input);
         XSSFSheet sheet=sheets.getSheetAt(0);
         //读取行数
         int rows=sheet.getPhysicalNumberOfRows();

         //读取列
         XSSFRow row=sheet.getRow(0);
         int cols=row.getPhysicalNumberOfCells();
         System.out.println(rows+""+cols);
         for (int i=1;i<rows;i++){
             row=sheet.getRow(i);
             int c0= (int)(row.getCell(0).getNumericCellValue());
             String c1=row.getCell(1).toString();
             String c2=(row.getCell(2).toString());
             int c3=(int)(row.getCell(3).getNumericCellValue());
             System.out.println(c0+"--"+c1+"--"+c2+"--"+c3);
             Student st=new Student();
             st.setStuid(c0);
             st.setName(c1);
             st.setAddress(c2);
             st.setClassid(c3);

             st.getClassid();
             int ok=studao.add(st);
         }
         sheets.close();
     }

}

