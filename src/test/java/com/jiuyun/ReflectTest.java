package com.jiuyun;

import com.jiuyun.entity.Student;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectTest {
     public static void main(String[] args) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
//         Class c= Student.class();   //类名
         Class c=new Student().getClass();  //对象名
//         Class c=class.forName("com");



         //获得所有Student里面的属性
         Field sx[]=c.getDeclaredFields();
         for(Field s:sx){
             System.out.println("属性"+s.getName());
         }
         //getMethods和getDeclaredMethods的区别          getMethods可以获得父类object继承的方法         getDeclaredMethods获得自定义的方法
//        Method m[]=c.getDeclaredMethods();
//         for (Method f:m){
//             System.out.println("方法"+f.getName());
//         }

         //获得其中一个方法(方法名,参数列表)
        Method m=c.getDeclaredMethod("setStuid", int.class);
         System.out.println(m.getName());
//         System.out.println(c);

         //调用方法
//         Student st=new Student();
//         st.setName("打野");

         //反射
         Object obj=c.newInstance();
         m.invoke(obj,1);
         System.out.println(obj);
    }
}
