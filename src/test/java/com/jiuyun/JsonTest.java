package com.jiuyun;

import com.google.gson.Gson;
import com.jiuyun.dao.StuentDao;
import com.jiuyun.dao.imol.StuentDaoImol;
import com.jiuyun.entity.Student;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

public class JsonTest {
    StuentDao stuDao= new StuentDaoImol();
    Gson gson=new Gson();
    @Test
    public  void  testStus() throws SQLException {
        //对象转string
        List list = stuDao.getUsersr();
        System.out.println(list);
    }
    @Test
    public void test2(){
        //string转成对象
        String s="{name:'李四', address:'湖北', age:17, stuid:2}";
        Student stu=gson.fromJson(s, Student.class);
        System.out.println(stu);
    }
}
