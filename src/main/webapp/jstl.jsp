<%@ page import="com.jiuyun.dao.ClassesDao" %>
<%@ page import="com.jiuyun.dao.imol.ClassesDaoImpl" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <%
        request.setAttribute("num",100);
        request.setAttribute("money",20000);
        request.setAttribute("week",4);
        String names[]={"提莫","人马","小丑","螳螂","EZ"};
        request.setAttribute("heros",names);
        ClassesDao dao=new ClassesDaoImpl();
        List clist=dao.getClasses();
        request.setAttribute("clist",clist);
        HashMap hsp=new HashMap();
    %>
</head>
<body>
    <h1> JSTL 测试</h1>
    <h3>数据 =${num}</h3>
    <h3>数据=<c:out value="${num}" default="没找到" escapeXml="false" /></h3>

    <h3>  IF </h3>
    <c:if test="${money >=5000}">
        你的工资${$money}可以找老婆了
    </c:if>
    <c:if test="${money < 5000}">
        可以打游戏了
    </c:if>
    <h3> choose , when  otherwise </h3>
    <c:choose>
        <c:when test="${week==1}">
            去看电影
        </c:when>
        <c:when test="${week==3}">
            去排位
        </c:when>
        <c:otherwise>
            搞学习
        </c:otherwise>
    </c:choose>
        <h3>forEach </h3>
    <c:forEach begin="1" end="10" var="k">
        <a href="#">${k}</a>
    </c:forEach>

    <c:forEach items="${heros}" var="obj" varStatus="i">
        <c:if test="i"></c:if>
        <br>${i.index}---${obj}---${i.count}
    </c:forEach>

    <%--集合加数组--%>
    <h1></h1>
    <c:forEach items="${clist}" var="ban">
        <br>${ban.classid}---${ban.classname}
    </c:forEach>
</body>
</html>