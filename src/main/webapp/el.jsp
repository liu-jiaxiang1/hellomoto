<%@ page import="com.jiuyun.entity.Student" %><%--
  Created by IntelliJ IDEA.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>EL  表达式</title>
</head>
<body>
        <%
            //模拟servlet
            request.setAttribute("height",100);
            request.setAttribute("handsome",90);
            //obj
            Student temo=new Student();
            temo.setStuid(90);
            temo.setName("提莫");
            temo.setAddress("1234");
            session.setAttribute("temo",temo);
        %>

        <h1>EL  表达式</h1>
        <h3>取值=${num}</h3>
        <h3>算术运算符= ${num gt 10}</h3>
        <h3>逻辑运算符  ${height> 50 or handsome > 80}</h3>
        <h3>空   ${empty handsome}</h3>
            <h3>对象属性  ${temo.name} 地址=${temo["address"]}</h3>
</body>
</html>
