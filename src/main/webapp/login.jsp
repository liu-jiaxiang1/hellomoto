<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/8/9
  Time: 15:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>chain.doFilter(request, response);
    <title>Title</title>
</head>
<body>
  <h1>国际化登入页</h1>
  <a href="login.jsp?lang=en">English</a>
  <a href="login.jsp?lang=zh">中文</a>
  <a href="login.jsp?lang=jp">J</a>
  <hr/>
  <c:if test="${param.lang==null || param.lang=='zh'}">
      <fmt:setLocale value="zh_CN"/>
  </c:if>

  <c:if test="${param.lang=='en'}">
      <fmt:setLocale value="en_US"/>
  </c:if>

  <c:if test="${param.lang=='jp'}">
      <fmt:setLocale value="ja_JP"/>
  </c:if>

  <fmt:bundle basename="sanzang">
  <form action="login" method="post">
     <fmt:message key="name"/> <input type="text" name="stname" /><br>
    <fmt:message key="pass"/> <input type="password" name="stpass" /><br>
      <input type="submit" value='<fmt:message key="login"/>'/>
  </form>
  </fmt:bundle>
</body>
</html>
