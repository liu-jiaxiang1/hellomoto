<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/8/9
  Time: 14:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Title</title>
    <%
        request.setAttribute("now",new Date());
        request.setAttribute("money",6000);
    %>
</head>
<body>

    <h1>jstl日期格式化</h1>
    $()
    <fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss E" />
    <h1>${money}</h1>
    <br><fmt:formatNumber type="CURRENCY" value="${money}" />
</body>
</html>
