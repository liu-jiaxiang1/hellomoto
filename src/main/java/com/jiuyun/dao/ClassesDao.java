package com.jiuyun.dao;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public interface ClassesDao {
    //获取所有的班级信息
    public List getClasses() throws SQLException;
    //获取某班的所有学生
    public  List getStudents(int classid) throws  SQLException;
    //获取某月某班作业情况
    public List getmouban(int classid,int year,int month) throws SQLException;
    //添加方法
    public boolean adduser(String name,int age,String address,int classid,int stuid) throws SQLException;
    //添加homework方法
    public boolean addhomework(int id, int studentid, Date ctime,String remark) throws SQLException;
}
