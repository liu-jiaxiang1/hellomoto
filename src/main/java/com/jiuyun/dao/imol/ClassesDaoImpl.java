package com.jiuyun.dao.imol;

import com.jiuyun.dao.ClassesDao;
import com.jiuyun.entity.Classes;
import com.jiuyun.entity.Homewwork;
import com.jiuyun.entity.Student;
import com.jiuyun.utils.AliPool;
import com.mysql.cj.Query;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class ClassesDaoImpl implements ClassesDao {
    QueryRunner runner=new QueryRunner();
//    Query query=new q
    @Override
    public List getClasses() throws SQLException {
        String sql="select * from bj";
        Connection conn= AliPool.getConn();
       List list=runner.query(conn,sql,new BeanListHandler<Classes>(Classes.class));
       conn.close();
        return list;
    }

    @Override
    public List getStudents(int classid) throws SQLException {
        String sql="select * from students where stuid=?";
        Connection conn=AliPool.getConn();
        List list=runner.query(conn,sql,new BeanListHandler<Student>(Student.class),classid);
        conn.close();
        return list;
    }

    @Override
    public List getmouban(int classid,int year,int mouth) throws SQLException {
        String sql="select s.name,h.* " +
                "from students s " +
                "INNER JOIN bj c ON c.classid=s.classid " +
                "INNER JOIN homework h on h.id=s.stuid " +
                "where c.classid=? and YEAR(ctime)=? AND MONTH(ctime)=?";
        Connection conn=AliPool.getConn();
        List list=runner.query(conn,sql,new BeanListHandler<Homewwork>(Homewwork.class),classid,year,mouth);
        conn.close();
        return list;
    }

    @Override
    public boolean adduser(String name, int age, String address, int classid, int stuid) throws SQLException {
        boolean z=false;
        int s=0;
        String sql="INSERT INTO students(name,age,address,stuid,classid) VALUES(?,?,?,?,?)";
        Connection conn=AliPool.getConn();
        s=runner.update(conn,sql,name,age,address,classid,stuid);
        conn.close();
        if(s!=0){
           z=true;
        }else{
            z=false;
        }
        return z;
    }

    @Override
    public boolean addhomework(int id, int studentid, Date ctime, String remark) throws SQLException {
        boolean z=false;
        int s=0;
        String sql="insert into homework(id,studentid,work,ctime,remark) VALUES(?,?,2,?,?)";
        Connection conn=AliPool.getConn();
        s=runner.update(conn,sql,id,studentid,ctime,remark);
        conn.close();
        if(s!=0){
            z=true;
        }else{
            z=false;
        }
        return z;
    }


}
