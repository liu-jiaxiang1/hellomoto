package com.jiuyun.dao.imol;

import com.jiuyun.dao.StuentDao;
import com.jiuyun.entity.Student;
import com.jiuyun.utils.AliPool;
import org.apache.commons.dbutils.QueryRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StuentDaoImol implements StuentDao {
    QueryRunner runner=new QueryRunner();
    @Override
    public List getUsersr() throws SQLException {
        String sql="select * from students";
        Connection conn= AliPool.getConn();
        PreparedStatement pt= conn.prepareCall(sql);
        ResultSet rs=pt.executeQuery();
        List list=new ArrayList();
        while (rs.next()){
            Student st=new Student();
            st.setName(rs.getString(1));
            st.setAge(rs.getInt(2));
            st.setAddress(rs.getString(3));
            st.setStuid(rs.getInt(4));
            list.add(st);//放入集合
        }
        conn.close();
        return list;
    }

    @Override
    public int add(Student st) throws SQLException {
        String sql="insert into students(stuid,name,address,classid) values(?,?,?,?)";
        Connection conn=AliPool.getConn();
        int s=runner.update(conn,sql,st.getStuid(),st.getName(),st.getAddress(),st.getClassid());
        return s;
    }
}
