package com.jiuyun.dao.imol;

import com.jiuyun.dao.AuthDao;
import com.jiuyun.entity.Auth;
import com.jiuyun.entity.Users;
import com.jiuyun.utils.AliPool;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class AuthDaoImpl implements AuthDao {
    QueryRunner runner=new QueryRunner();
    @Override
    public Users login(String name, String pass) throws SQLException {
        String sql="select * from user where uname=? and upass=?";
        Connection conn= AliPool.getConn();
//        Users user= runner.query(conn,sql,new BeanListHandler<Users>(Users.class),name,pass);
       Users user=runner.query(conn,sql,new BeanHandler<Users>(Users.class),name,pass);
        conn.close();
        return user;
    }

    @Override
    public List<Auth> mtauth(int uid) throws SQLException {
        String sql="select * from myauth where ";
        Connection conn= AliPool.getConn();
        List list=runner.query(conn,sql,new BeanListHandler<Auth>(Auth.class),uid);
        conn.close();
        return list;
    }
}
