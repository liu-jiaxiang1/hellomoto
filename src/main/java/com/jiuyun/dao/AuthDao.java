package com.jiuyun.dao;

import com.jiuyun.entity.Auth;
import com.jiuyun.entity.Users;

import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.util.List;

public interface AuthDao {
    //登入
    public Users login(String name,String pass) throws SQLException;
    //根据用户id来查某人的权限
    public List<Auth> mtauth(int id) throws SQLException;
}
