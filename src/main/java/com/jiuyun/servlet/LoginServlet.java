package com.jiuyun.servlet;

import com.jiuyun.dao.AuthDao;
import com.jiuyun.dao.imol.AuthDaoImpl;
import com.jiuyun.entity.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

//完成登入
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    AuthDao authDao=new AuthDaoImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        String name = request.getParameter("stname");
        String pass = request.getParameter("stpass");
        Users user =null;
        try {
            user = authDao.login(name,pass);
            if(user == null){
                System.out.println("登录失败");
            }else{
                //成功 session放个标志
                HttpSession session =  request.getSession();
                List list = authDao.mtauth( user.getUid() );
                session.setAttribute("LOGIN",user);
                session.setAttribute("auths",list);
                resp.sendRedirect("/taobao/index.jsp");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}
