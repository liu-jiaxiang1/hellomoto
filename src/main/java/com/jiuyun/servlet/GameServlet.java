package com.jiuyun.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("vip/*")
public class GameServlet extends HttpServlet {
    protected void tv(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("运行 收费视频()");
    }
    protected void adv(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("运行 免广告()");
    }
    protected void gift(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("运行 礼物()");
    }
}
