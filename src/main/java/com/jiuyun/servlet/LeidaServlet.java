package com.jiuyun.servlet;

import com.google.gson.Gson;
import com.jiuyun.vo.ChartData;
import com.jiuyun.vo.LeidaData;

import javax.naming.Name;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
@WebServlet("/leida")
public class LeidaServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }
    Gson gson=new Gson();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name[]={"销量","管理","信息技术","客服","研发","市场"};
        int data[]={4200, 3000, 20000, 35000, 50000, 18000};
        int sz[]={5000, 14000, 28000, 26000, 42000, 21000};
        int max[]={6500,16000,30000,38000,52000,25000};
       LeidaData cd=new LeidaData(sz,data,name,max);
        String s=gson.toJson(cd);
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("application/'json");
        PrintWriter out=resp.getWriter();
        out.print(s);//数据
        out.flush();
        out.close();
    }
}
