package com.jiuyun.servlet;

import com.google.gson.Gson;
import com.jiuyun.dao.StuentDao;
import com.jiuyun.dao.imol.StuentDaoImol;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/json")
public class JsonServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       doPost(req, resp);
    }
    StuentDao dao=new StuentDaoImol();
    Gson gson=new Gson();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String s="";
        try {
            List list=dao.getUsersr();
            s=gson.toJson(list);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("application/'json");
        PrintWriter out=resp.getWriter();
        out.print(s);
        out.flush();
        out.close();
    }
}
