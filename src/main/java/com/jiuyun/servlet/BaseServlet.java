package com.jiuyun.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

public class BaseServlet extends HttpServlet {
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String url=req.getRequestURI();
        int gang=url.lastIndexOf("/");
        String sname= url.substring(gang+1);
        System.out.println("运行了"+url);
        Class c=this.getClass();
        try {
            Method m=c.getDeclaredMethod(sname,HttpServletRequest.class, HttpServletResponse.class);
            m.invoke(this,req,resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    protected void add(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet");
    }
    protected void serch(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("serch");
    }
    protected void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("删除");
    }
    protected void xg(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("修改");
    }
}
