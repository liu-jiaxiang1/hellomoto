package com.jiuyun.servlet;

import com.google.gson.Gson;
import com.jiuyun.dao.ClassesDao;
import com.jiuyun.dao.imol.ClassesDaoImpl;
import com.jiuyun.service.ClassServies;
import com.jiuyun.service.impl.ClassServiesImpl;
import jdk.nashorn.internal.objects.NativeJSON;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@WebServlet("/homework/*")
public class ClsssServlet extends BaseServlet {
    ClassServies servies=new ClassServiesImpl();
    ClassesDao dao=new ClassesDaoImpl();
    Gson gson=new Gson();
    @Override
    protected void serch(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ClassServies cs=new ClassServiesImpl();
        HashMap ls=null;
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        String cid=req.getParameter("cid");
        int sid=Integer.parseInt(cid);

        String rq=req.getParameter("riqi");
        String sz[]=rq.split("-");
        int year=Integer.parseInt(sz[0]);
        int m=Integer.parseInt(sz[1]);
        try {
             ls=cs.homework(sid,year,m);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        req.setAttribute("hs",ls);
        req.getRequestDispatcher("/ad.jsp").forward(req,resp);
    }

    protected void ban(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, SQLException {
        List list=dao.getClasses();
        String s=gson.toJson(list);
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("application/json");
        PrintWriter out=resp.getWriter();
        out.print(s);//数据
        out.flush();
        out.close();
    }
    protected void xue(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, SQLException {
        String cid=req.getParameter("cid");
         List stus=dao.getStudents(Integer.parseInt(cid));
        String s=gson.toJson(stus);
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("application/json");
        PrintWriter out=resp.getWriter();
        out.print(s);//数据
        out.flush();
        out.close();
    }
    protected void tj(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, SQLException, ParseException {
        String cid=req.getParameter("cid");
        System.out.println("班级是"+cid);
        String name=req.getParameter("name");
        System.out.println("姓名是"+name);
        String sj=req.getParameter("sj");
        System.out.println("时间是"+sj);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Date ss=sdf.parse(sj);
        System.out.println("转换后的时间是"+ss);
        String yy=req.getParameter("yy");
        ClassesDao cs=new ClassesDaoImpl();
        boolean s=false;
        try {
            s=cs.addhomework(Integer.parseInt(cid),Integer.parseInt(cid),ss,yy);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(s);

        resp.setCharacterEncoding("utf-8");
        resp.setContentType("application/json");
        PrintWriter out=resp.getWriter();
        out.print(""+s);//数据
//        out.print("{\"code\":+s+}");
        out.flush();
        out.close();
    }
    @Override
    protected void add(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ClassesDao cs=new ClassesDaoImpl();
        boolean s=false;
        try {
            s=cs.adduser("小明",18,"深圳",2,7);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(s);
    }
}
