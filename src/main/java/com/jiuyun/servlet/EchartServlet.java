package com.jiuyun.servlet;

import com.google.gson.Gson;
import com.jiuyun.vo.ChartData;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
@WebServlet("/echart")
public class EchartServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }
    Gson gson=new Gson();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //本应该来自  数据库
        String title[]={"T shrit","羊毛衫","短裤","裤子","高跟鞋","袜子"};
        int data[]={5,12,26,40,22,17};
        ChartData cd=new ChartData(title,data);
        String s=gson.toJson(cd);
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("application/'json");
        PrintWriter out=resp.getWriter();
        out.print(s);//数据
        out.flush();
        out.close();
    }
}
