package com.jiuyun.servlet;

import com.jiuyun.dao.StuentDao;
import com.jiuyun.dao.imol.StuentDaoImol;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/user/*")
public class HelloServlet extends HttpServlet {
       StuentDao studao=new StuentDaoImol();
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List list=null;
        try {
            list=studao.getUsersr();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.setAttribute("users",list);
        req.getRequestDispatcher("index.jsp").forward(req,resp);
    }
    protected void cart(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("运行 购物车查询()");
    }
    protected void order(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("运行 订单查询()");
    }
    protected void info(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("运行 个人信息()");
    }
}
