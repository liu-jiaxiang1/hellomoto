package com.jiuyun.servlet;

import com.alibaba.druid.sql.visitor.functions.Substring;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;
@WebServlet("/hello/*")
public class ReflectServlet extends BaseServlet {

    protected void add(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet");
    }
    protected void serch(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("serch");
    }
    protected void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("删除");
    }
    protected void xg(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("修改");
    }
}
