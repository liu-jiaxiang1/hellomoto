package com.jiuyun.entity;

import java.util.Date;

//作业情况
public class Homewwork {
    int id;
    int studentid;
    int work;//作业
    Date ctime;//那一天
    String remark;//原因
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStudentid() {
        return studentid;
    }

    public void setStudentid(int studentid) {
        this.studentid = studentid;
    }

    public int getWork() {
        return work;
    }

    public void setWork(int work) {
        this.work = work;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "Homewwork{" +
                "id=" + id +
                ", studentid=" + studentid +
                ", work=" + work +
                ", ctime=" + ctime +
                ", remark='" + remark + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
