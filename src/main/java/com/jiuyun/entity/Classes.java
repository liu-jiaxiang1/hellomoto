package com.jiuyun.entity;
//班级表
public class Classes {
    int classid;
    String classname;

    public int getClassid() {
        return classid;
    }

    public void setClassid(int classid) {
        this.classid = classid;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    @Override
    public String toString() {
        return "Homewwork{" +
                "classid=" + classid +
                ", classname='" + classname + '\'' +
                '}';
    }
}
