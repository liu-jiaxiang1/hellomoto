package com.jiuyun.service.impl;

import com.jiuyun.dao.ClassesDao;
import com.jiuyun.dao.StuentDao;
import com.jiuyun.dao.imol.ClassesDaoImpl;
import com.jiuyun.entity.Homewwork;
import com.jiuyun.entity.Student;
import com.jiuyun.service.ClassServies;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class ClassServiesImpl implements ClassServies {
    ClassesDao dao=new ClassesDaoImpl();
    @Override
    public HashMap<String, int[]> homework(int classid,int year,int month) throws SQLException {
        int days[]={30,28,31,30,31,30,31,31,30,31,30,31};
        HashMap<String, int[]> info=new HashMap<String, int[]>();
        if(year%4==0&&year%100!=0||year%400==0) {
            days[1] = 29;
        }
        //本月天数加一
        int num=days[month-1];
        //没作业的列表
        List<Homewwork> works=dao.getmouban(1,2020,4);
        //等到所有学生
        List<Student> stus=dao.getStudents(classid);
        for(Student s:stus){
            info.put(s.getName(),new int[num]);
        }
        for(Homewwork h:works){
            String name=h.getName();//works里面的姓名赋值给name
            int t=h.getCtime().getDate();//获得天
            int rq[]=info.get(name);//用name查找hashMap里面对应的int数组
            rq[t-1]=1;//没做的标志

        }
        return info;
    }
}
