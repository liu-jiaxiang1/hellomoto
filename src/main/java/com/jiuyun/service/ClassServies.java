package com.jiuyun.service;

import java.sql.SQLException;
import java.util.HashMap;

/**
 * 完成班级作业查询
 */

public interface ClassServies {
        public HashMap<String,int[]> homework(int classid,int year,int month) throws SQLException;
}
