package com.jiuyun.filter;

import com.jiuyun.entity.Auth;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebFilter({"/users/*","/vip/*"})
public class Filter_Auth implements Filter {
    public void init(FilterConfig config) throws ServletException {
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        System.out.println("正在 检查权限....");
        HttpServletRequest req = (HttpServletRequest)request;
        HttpSession session = req.getSession();
        String path = req.getRequestURI();
        List<Auth> auths = (List)session.getAttribute("auths");

        //System.out.println("你正在访问的路径=" + path); /taobao/vip/gift
        //System.out.println("你可以访问的路径=" + auths); /vip/gift
        boolean find = false;
        for(Auth a : auths){
            if(path.endsWith(a.getAuthPath())){
                find =true;
                break;
            }
        }

        if(find){
            chain.doFilter(request,response);
        }else{
            System.out.println("非法访问 servlet ...");
        }
    }
}
