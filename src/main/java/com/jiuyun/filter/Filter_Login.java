package com.jiuyun.filter;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("*.do")
public class Filter_Login implements Filter {
    public void init(FilterConfig config) throws ServletException {
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        System.out.println("登入正在过滤");
        HttpServletRequest request1=(HttpServletRequest)request;
        HttpSession session=request1.getSession();
        Object name= session.getAttribute("name");
        if(name==null){
            System.out.println("非法用户");
        }else{
            chain.doFilter(request, response);
        }

    }
}
