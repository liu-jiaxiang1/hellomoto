package com.jiuyun.vo;

public class LeidaData {
        int sz[];
        int data[];
        String name[];
        int max[];

    public LeidaData(int[] sz, int[] data, String name[], int max[]) {
        this.sz = sz;
        this.data = data;
        this.name = name;
        this.max = max;
    }

    public int[] getSz() {
        return sz;
    }

    public void setSz(int[] sz) {
        this.sz = sz;
    }

    public int[] getData() {
        return data;
    }

    public void setData(int[] data) {
        this.data = data;
    }

    public String[] getName() {
        return name;
    }

    public void setName(String[] name) {
        this.name = name;
    }

    public int[] getMax() {
        return max;
    }

    public void setMax(int[] max) {
        this.max = max;
    }
}
